package com.iptiq.loadbalancer;

import com.iptiq.loadbalancer.provider.Provider;

public interface LoadBalancer {
    String get() throws InterruptedException;
    void addProvider(Provider providerUUID);
    void removeProvider(String providerUUID);
    void stop();

}
