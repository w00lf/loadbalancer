package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.provider.Provider;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimerTask;

public class HeartbeatTask extends TimerTask {
    private ProviderList providersList;
    //Map containing the providers and their heartbeats
    private Map<Provider, Integer> providerHeartBeats = new HashMap<>();

    HeartbeatTask(ProviderList providersList) {
        this.providersList = providersList;
    }

    @Override
    public void run() {
        final Iterator<Provider> fullListIterator = providersList.getFullListIterator();
        while (fullListIterator.hasNext()) {
            final Provider nextProvider = fullListIterator.next();
            providerHeartBeats.compute(nextProvider, (provider, requiredHeartbeats) -> {
                if (nextProvider.check()) {
                    if (requiredHeartbeats == null) {
                        return 0;
                    }
                    switch (requiredHeartbeats) {
                        case 2:
                            return 1;
                        case 1:
                            providersList.markAlive(nextProvider);
                        default:
                            return 0;
                    }
                } else {
                    if (requiredHeartbeats == null || requiredHeartbeats == 0) {
                        providersList.markDead(nextProvider);
                    }
                    return 2;
                }
            });
        }
    }
}
