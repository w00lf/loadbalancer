package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.provider.Provider;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class RoundRobinLoadBalancer extends BaseLoadBalancer {
    private AtomicInteger index = new AtomicInteger(0);

    public RoundRobinLoadBalancer(int heartbeatSeconds, int maxProviderRequests) {
        super(heartbeatSeconds, maxProviderRequests);
    }

    @Override
    protected Provider chooseProvider() {
        Optional<Provider> provider;
        do {
            final int aliveListSize = providerList.getAliveListSize();
            int index = getNextIndex(aliveListSize);
            provider = providerList.safeGetAliveProvider(index);
        } while (provider.isEmpty());
        return provider.get();
    }

    private int getNextIndex(int aliveListSize) {
        int oldValue, newValue;
        do {
            oldValue = index.get();
            newValue = (oldValue >= aliveListSize - 1) ? 0 : oldValue + 1;
        } while (!index.compareAndSet(oldValue, newValue));
        return oldValue;
    }
}
