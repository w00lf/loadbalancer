package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.provider.Provider;

import java.util.Optional;
import java.util.Random;

public class RandomLoadBalancer extends BaseLoadBalancer {
    private final Random RANDOM = new Random(System.currentTimeMillis());

    public RandomLoadBalancer(int heartbeatSeconds, int maxProviderRequests) {
        super(heartbeatSeconds, maxProviderRequests);
    }

    @Override
    protected Provider chooseProvider() {
        Optional<Provider> provider;
        do {
            final int aliveListSize = providerList.getAliveListSize();
            int randomIndex = RANDOM.nextInt(aliveListSize);
            provider = providerList.safeGetAliveProvider(randomIndex);
        } while (provider.isEmpty());
        return provider.get();
    }
}
