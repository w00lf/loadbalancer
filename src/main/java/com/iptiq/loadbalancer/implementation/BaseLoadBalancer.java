package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.LoadBalancer;
import com.iptiq.loadbalancer.exceptions.TooManyActiveCallsException;
import com.iptiq.loadbalancer.provider.Provider;

import java.util.Timer;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BaseLoadBalancer implements LoadBalancer {
    private final int maxProviderRequests;
    protected ProviderList providerList = new ProviderList();
    private AtomicInteger activeCalls = new AtomicInteger(0);
    private Timer timer;

    protected BaseLoadBalancer(int heartbeatSeconds, int maxProviderRequests) {
        this.maxProviderRequests = maxProviderRequests;
        HeartbeatTask heartbeatTask = new HeartbeatTask(providerList);
        timer = new Timer("Heartbeat");
        long delay = heartbeatSeconds * 1000L;
        timer.schedule(heartbeatTask, delay);
    }

    @Override
    public void addProvider(Provider provider) {
        providerList.addProvider(provider);
    }

    @Override
    public void removeProvider(String providerUUID) {
        providerList.removeProvider(providerUUID);
    }

    @Override
    public void stop() {
        timer.cancel();
    }

    @Override
    public String get() {
        final int aliveListSize = providerList.getAliveListSize();
        if (aliveListSize > 0 && activeCalls.get() >= maxProviderRequests * aliveListSize) {
            throw new TooManyActiveCallsException();
        }

        Provider provider = chooseProvider();
        activeCalls.incrementAndGet();
        final String uuid = provider.get();
        activeCalls.decrementAndGet();
        return uuid;
    }

    protected abstract Provider chooseProvider();
}
