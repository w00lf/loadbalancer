package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.exceptions.MaxProvidersReached;
import com.iptiq.loadbalancer.exceptions.NoActiveProvidersException;
import com.iptiq.loadbalancer.provider.Provider;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class ProviderList {
    private List<Provider> providerList = new CopyOnWriteArrayList<>();
    private List<Provider> aliveList = new ArrayList<>();
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock writeLock = readWriteLock.writeLock();
    private final Lock readLock = readWriteLock.readLock();
    private static final int MAX_INSTANCE = 10;

    void addProvider(Provider provider) {
        if (providerList.size() < MAX_INSTANCE) {
            providerList.add(provider);
            writeLock.lock();
            aliveList.add(provider);
            writeLock.unlock();
        } else {
            throw new MaxProvidersReached();
        }
    }

    void removeProvider(String providerUUID) {
        final UUID uuid = UUID.fromString(providerUUID);
        providerList.stream()
                .filter(provider -> provider.getUUID().equals(uuid))
                .findFirst()
                .ifPresent(provider -> providerList.remove(provider));
        aliveList.stream()
                .filter(provider -> provider.getUUID().equals(uuid))
                .findFirst()
                .ifPresent(provider -> {
                    writeLock.lock();
                    aliveList.remove(provider);
                    writeLock.unlock();
                });
    }

    Iterator<Provider> getFullListIterator() {
        return providerList.iterator();
    }

    void markAlive(Provider provider) {
        aliveList.add(provider);
    }

    void markDead(Provider provider) {
        aliveList.remove(provider);
    }

    int getAliveListSize() {
        return aliveList.size();
    }

    /**
     * Method to get the active provider at a certain index only if the index is within range.
     * Analogous to AtomicInteger.compareAndSet()).
     * Used to avoid ArrayIndexOutOfBoundsException if the list was shortened in another thread.
     * @param index the desired list index
     * @return a Provider wrapped in an Optional if the index is valid, an empty Optional otherwise
     */
    Optional<Provider> safeGetAliveProvider(int index) {
        readLock.lock();
        try {
            if (aliveList.isEmpty()) {
                throw new NoActiveProvidersException();
            }
            if (index < aliveList.size()) {
                return Optional.of(aliveList.get(index));
            }
            return Optional.empty();
        } finally {
            readLock.unlock();
        }
    }
}
