package com.iptiq.loadbalancer.provider;

import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

public class Provider {
    private static final Logger LOGGER = Logger.getLogger(Provider.class.getName());

    private Random randomExecutionTime = new Random(System.currentTimeMillis());
    private UUID uuid = UUID.randomUUID();
    private String uuidString = uuid.toString();
    private Boolean alive = true;

    public String get() {
        try {
            Thread.sleep(randomExecutionTime.nextInt(500));
        } catch (InterruptedException e) {
            return uuidString;
        }
        LOGGER.info("Provider: Invoked get " + uuidString);
        return uuidString;
    }

    public UUID getUUID() {
        return uuid;
    }

    public Boolean check() {
        return alive;
    }

    public void setAlive(Boolean alive) {
        this.alive = alive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Provider provider = (Provider) o;
        return uuid.equals(provider.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
