package com.iptiq.loadbalancer.exceptions;

public class NoActiveProvidersException extends RuntimeException {

    public NoActiveProvidersException() {
        super("No active providers found");
    }
}
