package com.iptiq.loadbalancer.exceptions;

public class TooManyActiveCallsException extends RuntimeException {
    public TooManyActiveCallsException() {
        super("Too many active calls");
    }
}
