package com.iptiq.loadbalancer.exceptions;

public class MaxProvidersReached extends RuntimeException {
    public MaxProvidersReached(){
        super("Max number of providers reached");
    }
}
