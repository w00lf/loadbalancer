package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.provider.Provider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

class RandomLoadBalancerTest {
    private RandomLoadBalancer randomLoadBalancer;

    @BeforeEach
    void setUp() {
        randomLoadBalancer = new RandomLoadBalancer(1,10);
        IntStream.range(0,10).forEach(value -> randomLoadBalancer.addProvider(new Provider()));
    }

    @Test
    void testRandom() throws InterruptedException {
        final String uuid1 = randomLoadBalancer.get();
        Assertions.assertNotNull(uuid1);
    }
}