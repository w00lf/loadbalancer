package com.iptiq.loadbalancer.implementation;

import com.iptiq.loadbalancer.exceptions.NoActiveProvidersException;
import com.iptiq.loadbalancer.exceptions.TooManyActiveCallsException;
import com.iptiq.loadbalancer.provider.Provider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RoundRobinLoadBalancerTest {
    private RoundRobinLoadBalancer roundRobinLoadBalancer;

    @BeforeEach
    void setUp() {
        roundRobinLoadBalancer = new RoundRobinLoadBalancer(1,10);
        IntStream.range(0, 10).forEach(value -> roundRobinLoadBalancer.addProvider(new Provider()));
    }

    @Test
    void testRoundRobin() {
        final String call1 = roundRobinLoadBalancer.get();
        IntStream.range(0, 9).forEach(value -> roundRobinLoadBalancer.get());
        final String call11 = roundRobinLoadBalancer.get();
        Assertions.assertEquals(call1, call11);
    }


    @Test
    void testNoActiveProviders() {
        Provider testProvider = new Provider();
        RoundRobinLoadBalancer roundRobinLoadBalancerOfOne = new RoundRobinLoadBalancer(1,10);
        roundRobinLoadBalancerOfOne.addProvider(testProvider);
        testProvider.setAlive(false);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertThrows(NoActiveProvidersException.class, roundRobinLoadBalancerOfOne::get);
    }

    @Test
    void testTooManyCallsException() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(100000);
        roundRobinLoadBalancer = new RoundRobinLoadBalancer(1,10);
        IntStream.range(0, 2).forEach(value -> roundRobinLoadBalancer.addProvider(new Provider()));
        final List<Future<Object>> futures = executor.invokeAll(Stream.generate(() -> (Callable<Object>) () -> roundRobinLoadBalancer.get()).limit(100).collect(Collectors.toList()));
        executor.shutdown();

        while(!executor.awaitTermination(100, TimeUnit.MILLISECONDS)){
            System.out.println("Still waiting");
        }
        final Optional<Boolean> tooMannyCallsExceptions = futures.stream().map(future -> {
            try {
                future.get();
                return false;
            } catch (InterruptedException e) {
                return false;
            } catch (ExecutionException e) {
                return e.getCause() instanceof TooManyActiveCallsException;
            }
        }).filter(aBoolean -> aBoolean).findAny();
        assertTrue(tooMannyCallsExceptions.isPresent());
    }
}