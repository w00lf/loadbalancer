# (H)ip(no)tiq Load Balancer 🤩

This is an implementation of a simple load balancer that supports two algorithms:
1. random instance;
2. round-robin.

## How to use

You can choose from either the [RandomLoadBalancer](src/main/java/com/iptiq/loadbalancer/implementation/RandomLoadBalancer.java) 
or the [RoundRobinLoadBalancer](src/main/java/com/iptiq/loadbalancer/implementation/RoundRobinLoadBalancer.java).
You will need to provide it with 2 parameters:
- the time interval (in seconds) at which to check the health of the providers;
- the maximum number of requests that can be sent to a single provider. 

To see it working, just instantiate any number of [Provider](src/main/java/com/iptiq/loadbalancer/provider/Provider.java)
instances and add them to the load balancer. 
You can then use the `LoadBalancer.get()` method to invoke the same call on the underlying providers according to the chosen algorithm.

To toggle provider availability , use `Provider.setAlive()`. 
The load balancer regularly checks these instances and all unavailable instances will be skipped from invocation until 2 heartbeats return successfully.

You can also completely remove providers from the load balancer to never even be checked again.

## Implementation details

Both load balancers implement the [LoadBalancer](src/main/java/com/iptiq/loadbalancer/LoadBalancer.java) interface.
Common functionality is in encapsulated by an abstract class. 
The concrete implementations differ only in the specific algorithm to choose an active instance (i.e. the `chooseProvider` method).

For keeping track of active instances, we opted for keeping a separate list that gets updated based on heartbeat checks.
This way, we optimize for frequent reads (calls to the `LoadBalancer.get()`) and less frequent writes (providers going on- and offline).